$(document).ready(function() {
    // "https://api.themoviedb.org/3/list/107803?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    let peliculas = [{
        tipo: 'peliculas-populares',
        url: 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES'
    }, {
        tipo: 'peliculas-accion',
        url: 'https://api.themoviedb.org/3/discover/movie?with_genres=28&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES'
    }, {
        tipo: 'peliculas-comedia',
        url: 'https://api.themoviedb.org/3/discover/movie?with_genres=35&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES'
    }, {
        tipo: 'peliculas-fantasia',
        url: 'https://api.themoviedb.org/3/discover/movie?with_genres=14&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES'
    }, {
        tipo: 'peliculas-drama',
        url: 'https://api.themoviedb.org/3/discover/movie?with_genres=18&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES'
    }]

    peliculas.forEach(pelicula => {
        $.ajax({
            method: "GET",
            url: pelicula.url,
            success: function(par) {
                var deslizable = ''
                var contenido = ''
                var prueba = ''
                var longitud = par.results.length
                $.each(par.results, function(i, item) {
                    obtencionEnlace(item.id, function(res) {
                        prueba = res
                    });
                    let enlaceFoto = item.poster_path !== null ? 'https://image.tmdb.org/t/p/original' + item.poster_path : '';
                    if ((i % 4) === 0) {
                        if (i !== 0) {
                            contenido += '<div class="carousel-item margenes">' +
                                '<div class="row">';
                        } else {
                            contenido += '<div class="carousel-item margenes active">' +
                                '<div class="row">';
                        }
                    }
                    contenido += '<div class="col-md-3">' +
                        '<img style="max-width:99%;" onclick="descripcion(\'' + item.title.replace(/["']/g, "") + '\',\'' + item.overview.replace(/["']/g, "") + '\',\'' + item.poster_path + '\',\'' + item.release_date + '\',\'' + item.vote_average + '\',\'' + item.poster_path + '\',\'' + prueba + '\');" type="button" data-toggle="modal" data-target="#exampleModalCenter" src="' + enlaceFoto + '" alt="' + item.title + '">' +
                        '</div>';

                    if ((i % 4) === 3 || i === longitud - 1) {
                        contenido += '</div>' +
                            '</div>';
                    }
                });
                var resultado = '<div class="row blog">' +
                    '<div class="col-md-12">' +
                    '<div id="carousel' + pelicula.tipo + '" class="carousel slide blog" data-ride="carousel">' +
                    '<div class="carousel-inner">' +
                    contenido +
                    '</div>' +
                    '<a class="carousel-control-prev flecha" href="#carousel' + pelicula.tipo + '" role="button" data-slide="prev">' +
                    '<span class="carousel-control-prev-icon" aria-hidden="true"></span>' +
                    '<span class="sr-only">Previous</span>' +
                    '</a>' +
                    '<a class="carousel-control-next flecha" href="#carousel' + pelicula.tipo + '" role="button" data-slide="next">' +
                    '<span class="carousel-control-next-icon" aria-hidden="true"></span>' +
                    '<span class="sr-only">Next</span>' +
                    '</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                $('#' + pelicula.tipo).append(resultado);
            },
            error: function(par) {
                alert("Datos no encontrados");
            }
        })
    });
});

$('.carousel').carousel('pause')

$("#busqueda").on('click', function() {
    let entrada = document.getElementById("contenido").value;
    let url = "https://api.themoviedb.org/3/search/movie?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES&query=" + entrada + "&page=1&include_adult=true"
    var resultado = ''
    entrada = entrada.replace(/ /g, "")
    peticionSinCarrusel(url, entrada, function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

function vaciar(idpadre) {
    var array_nodos = document.getElementById(idpadre).childNodes;
    for (var i = 0; i < array_nodos.length; i++) {
        if (array_nodos[i].nodeType == 1) {
            array_nodos[i].parentNode.removeChild(array_nodos[i]);
        }
    }
};

$("#favoritas").on('click', function() {
    let entrada = document.getElementById("contenido").value;
    let url = "https://api.themoviedb.org/3/list/107803?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Favoritas", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#Accion").on('click', function() {
    let url = "https://api.themoviedb.org/3/discover/movie?with_genres=28&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Accion", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#Fantasia").on('click', function() {
    let url = "https://api.themoviedb.org/3/discover/movie?with_genres=14&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Fantasia", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#Drama").on('click', function() {
    let url = "https://api.themoviedb.org/3/discover/movie?with_genres=18&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Drama", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#Ficcion").on('click', function() {
    let url = "https://api.themoviedb.org/3/discover/movie?with_genres=878&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Ficcion", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#Comedia").on('click', function() {
    let url = "https://api.themoviedb.org/3/discover/movie?with_genres=35&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Comedia", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#Aventura").on('click', function() {
    let url = "https://api.themoviedb.org/3/discover/movie?with_genres=12&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Aventura", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#Animation").on('click', function() {
    let url = "https://api.themoviedb.org/3/discover/movie?with_genres=16&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Animation", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#Familiar").on('click', function() {
    let url = "https://api.themoviedb.org/3/discover/movie?with_genres=10751&api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES"
    var resultado = ''
    peticionSinCarrusel(url, "Familiar", function(res) {
        resultado = res
        vaciar('busquedas');
        $('#busquedas').append(resultado);
    })
});

$("#contenido").keypress(function(event) {
    if (event.which == 13) {
        let entrada = document.getElementById("contenido").value;
        let url = "https://api.themoviedb.org/3/search/movie?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES&query=" + entrada + "&page=1&include_adult=true"
        var resultado = ''
        peticionSinCarrusel(url, entrada, function(res) {
            resultado = res
            vaciar('busquedas');
            $('#busquedas').append(resultado);
        })
    }
});


function descripcion(titulo, descripcion, foto, fecha, votacion, foto, enlace) {
    vaciar('exampleModalCenter');
    let color = ''
    if (votacion >= 7) {
        color = 'text-success'
    } else if (votacion < 5) {
        color = 'text-danger'
    } else if (votacion >= 5) {
        color = 'text-warning'
    }
    let frame = enlace !== "No hay datos" ? '<iframe class="mx-auto center-block" width="460" height="255" src="' + enlace + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' : '<p class="card-text"><small class="text-muted">Video no disponible</small></p>';
    console.log(foto)
    let imagen = foto !== null ? 'https://image.tmdb.org/t/p/original' + foto : ''
    console.log(imagen, titulo)
    const resultado =
        '<div class="modal-dialog modal-lg" role="document">' +
        '<div class="modal-content">' +
        '<div class="modal-header degradado">' +
        '<h5 class="modal-title" id="exampleModalCenterTitle" style="color: white">' + titulo + '</h5>' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button>' +
        '</div>' +
        '<div class="modal-body" style="color: black; border:none;">' +
        '<div class="card mb-3">' +
        '<div class="row no-gutters">' +
        '<div class="col-md-4">' +
        '<a target=”_blank" href="' + imagen + '"><img src="' + imagen + '" class=" m-2 card-img" alt="' + titulo + '"> </a>' +
        '</div>' +
        '<div class="col-md-8">' +
        '<div class="card-body">' +
        '<p class="card-text">' + descripcion + '</p>' +
        '<p class="card-text"><small class="text-muted">Director: No hay datos de bbdd</small></p>' +
        '<p class="card-text"><small class="text-muted">Reparto: No hay datos</small></p>' +
        '<p class="card-text"><small class="text-muted"><span class=' + color + '>Valoracion:  ' + votacion + '</span></small></p>' +
        '<p class="card-text"><small class="text-muted">Fecha de lanzamiento: ' + fecha + '</small></p>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="mt-2 mx-auto">' + frame + '</div>' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer" style="padding:0rem 1rem 1rem 1rem; border:none;">' +
        '<button type="button" class="btn btn-outline-dark" data-dismiss="modal">Atrás</button>' +
        '<button type="button" class="btn btn-outline-warning text-dark" data-dismiss="modal">Añadir a favoritos</button>' +
        '</div>' +
        '</div>' +
        '</div>'
    $('#exampleModalCenter').append(resultado);
};


function peticionSinCarrusel(uri, entrada, callback) {
    var contenido = ''
    var resultado = ''
    $.ajax({
        method: "GET",
        url: uri,
        success: function(par) {
            var array = []
            if (par.items) {
                array = par.items
            } else if (par.results) {
                array = par.results
            }
            var longitud = array.length
            $.each(array, function(i, item) {
                obtencionEnlace(item.id, function(res) {
                    prueba = res
                });
                let enlaceFoto = item.poster_path !== null ? 'https://image.tmdb.org/t/p/original' + item.poster_path : '';
                if ((i % 4) === 0) {
                    if (i !== 0) {
                        contenido += '<div class="margenes">' +
                            '<div class="row">';
                    } else {
                        contenido += '<div class="margenes">' +
                            '<div class="row">';
                    }
                }
                contenido += '<div class="col-md-3">' +
                    '<img style="max-width:99%; margin-bottom: 10%" onclick="descripcion(\'' + item.title.replace(/["']/g, "") + '\',\'' + item.overview.replace(/["']/g, "") + '\',\'' + item.poster_path + '\',\'' + item.release_date + '\',\'' + item.vote_average + '\',\'' + item.poster_path + '\',\'' + prueba + '\');" type="button" data-toggle="modal" data-target="#exampleModalCenter" src="' + enlaceFoto + '" alt="' + item.title + '">' +
                    '</div>';

                if ((i % 4) === 3 || i === longitud - 1) {
                    contenido += '</div>' +
                        '</div>';
                }
            });
            resultado =
                '<section class="carousel-populares">' +
                '<div class="clasificacion">' +
                '<h1>' + entrada + '</h1>' +
                '</div>' +
                '<div class="row blog">' +
                '<div class="col-md-12">' +
                '<div id="sincarousel' + entrada + '">' +
                '<div>' +
                contenido +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</section>'
            callback(resultado)
        },
        error: function(par) {
            alert("Datos no encontrados");
        }
    });
}

function peticion(uri, entrada, callback) {
    var contenido = ''
    var resultado = ''
    $.ajax({
        method: "GET",
        url: uri,
        success: function(par) {
            var array = []
            if (par.items) {
                array = par.items
            } else if (par.results) {
                array = par.results
            }
            var longitud = array.length
            $.each(array, function(i, item) {
                if ((i % 4) === 0) {
                    if (i !== 0) {
                        contenido += '<div class="carousel-item margenes">' +
                            '<div class="row">';

                    } else {
                        contenido += '<div class="carousel-item margenes active">' +
                            '<div class="row">';
                    }
                }
                contenido += '<div class="col-md-3">' +
                    '<img style="max-width:99%;" onclick="descripcion(\'' + item.title.replace(/["']/g, "") + '\',\'' + item.overview.replace(/["']/g, "") + '\',\'' + item.poster_path + '\',\'' + item.release_date + '\');" type="button" data-toggle="modal" data-target="#exampleModalCenter" src="https://image.tmdb.org/t/p/original' + item.poster_path + '">' +
                    '</div>';

                if ((i % 4) === 3 || i === longitud - 1) {
                    contenido += '</div>' +
                        '</div>';
                }
            });
            resultado =
                '<section class="carousel-populares">' +
                '<div class="clasificacion">' +
                '<h1>' + entrada + '</h1>' +
                '</div>' +
                '<div class="row blog">' +
                '<div class="col-md-12">' +
                '<div id="carousel' + entrada + '" class="carousel slide blog" data-ride="carousel">' +
                '<div class="carousel-inner">' +
                contenido +
                '</div>' +
                '<a class="carousel-control-prev flecha" href="#carousel' + entrada + '" role="button" data-slide="prev">' +
                '<span class="carousel-control-prev-icon" aria-hidden="true"></span>' +
                '<span class="sr-only">Previous</span>' +
                '</a>' +
                '<a class="carousel-control-next flecha" href="#carousel' + entrada + '" role="button" data-slide="next">' +
                '<span class="carousel-control-next-icon" aria-hidden="true"></span>' +
                '<span class="sr-only">Next</span>' +
                '</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</section>'
            callback(resultado)
        },
        error: function(par) {
            alert("Datos no encontrados");
        }
    });
}

// Obtiene el enlace de los videos
function obtencionEnlace(id, callback) {
    var contenido = ''
    var resultado = ''
    $.ajax({
        method: 'GET',
        url: 'https://api.themoviedb.org/3/movie/' + id + '/videos?api_key=831e08f1af6a358dffb3e2b2790c7f3a&language=es-ES',
        async: false,
        success: function(par) {
            resultado = par.results[0] !== undefined ? 'https://www.youtube.com/embed/' + par.results[0].key : 'No hay datos';
            callback(resultado)
        },
        error: function(par) {
            alert("Datos no encontrados");
        }
    });
}