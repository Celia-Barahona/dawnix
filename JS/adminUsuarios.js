$(document).ready(function() {
    // Administrar usuarios
    $('.lapiz').on('click', function() {
        var $admin = $(this).parents('.atributo-botones').siblings('.atributo-administrador').text();
        var $nombre = $(this).parents('.atributo-botones').siblings('.atributo-nombre').text();
        var $email = $(this).parents('.atributo-botones').siblings('.atributo-email').text();
        var $contraseña = $(this).parents('.atributo-botones').siblings('.atributo-contraseña').text();

        var $modal;
        $('#modal-modificar-usuario').find('.form-control.separador').each(function() {
            $modal = $(this).prop('name');
            if ($modal == 'admin') {
                $(this).prop('placeholder', $admin);
            } else if ($modal == 'nombre') {
                $(this).prop('placeholder', $nombre);
            } else if ($modal == 'email') {
                $(this).prop('placeholder', $email);
            } else {
                $(this).prop('placeholder', $contraseña);
            }
        });
    });

    // Buscador en tiempo real
    (function($) {
        $('#filtrar').keyup(function() {
            var rex = new RegExp($(this).val(), 'i');
            $('.buscar tr').hide();
            $('.buscar tr').filter(function() {
                return rex.test($(this).find('.atributo-nombre').text());
            }).show();
        })
    }(jQuery));
});