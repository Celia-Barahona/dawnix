$(document).ready(function() {
    // Administrar películas
    $('.lapiz').on('click', function() {
        let $titulo = $(this).parents('.atributo-botones').siblings('.atributo-titulo').text();
        let $categoria = $(this).parents('.atributo-botones').siblings('.atributo-categoria').text();
        let $descripcion = $(this).parents('.atributo-botones').siblings('.atributo-descripcion').text();
        let $año = $(this).parents('.atributo-botones').siblings('.atributo-año').text();
        let $director = $(this).parents('.atributo-botones').siblings('.atributo-director').text();
        let $reparto = $(this).parents('.atributo-botones').siblings('.atributo-reparto').text();
        let $valoracion = $(this).parents('.atributo-botones').siblings('.atributo-valoracion').text();
        let $urlVideo = $(this).parents('.atributo-botones').siblings('.atributo-url-video').text();
        let $urlPortada = $(this).parents('.atributo-botones').siblings('.atributo-url-portada').text();

        var $modal;
        $('#modal-modificar-pelicula').find('.form-control.separador').each(function() {
            $modal = $(this).prop('name');
            if ($modal == 'titulo') {
                $(this).prop('placeholder', $titulo);
            } else if ($modal == 'categoria') {
                $(this).prop('placeholder', $categoria);
            } else if ($modal == 'descripcion') {
                $(this).prop('placeholder', $descripcion);
            } else if ($modal == 'año') {
                $(this).prop('placeholder', $año);
            } else if ($modal == 'director') {
                $(this).prop('placeholder', $director);
            } else if ($modal == 'reparto') {
                $(this).prop('placeholder', $reparto);
            } else if ($modal == 'valoracion') {
                $(this).prop('placeholder', $valoracion);
            } else if ($modal == 'URLVideo') {
                $(this).prop('placeholder', $urlVideo);
            } else {
                $(this).prop('placeholder', $urlPortada);
            }
        });
    });

    // Buscador en tiempo real
    (function($) {
        $('#filtrar').keyup(function() {
            var rex = new RegExp($(this).val(), 'i');
            $('.buscar tr').hide();
            $('.buscar tr').filter(function() {
                return rex.test($(this).find('.atributo-titulo').text());
            }).show();
        })
    }(jQuery));
});